import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';



@NgModule({
  declarations: [
    LoginDialogComponent
  ],
  imports: [
    CommonModule
  ]
})
export class LoginDialogModule { }
